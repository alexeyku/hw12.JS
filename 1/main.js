function circleCreate(){
	let input = document.createElement('input')
	input.id = 'inp1'
	input.placeholder = 'Введите диаметр'

	let input2 = document.createElement('input')
	input2.id = 'inp2'
	input2.placeholder = 'Введите цвет'

	let button2 = document.createElement('button')
	button2.id = 'button-2'
	button2.innerHTML = 'Нарисовать'


	document.getElementById('button-1').onclick = function(){
		document.getElementById('button-1').style = 'display:none;'
		document.body.appendChild(input)
		document.body.appendChild(input2)
		document.body.appendChild(button2)

			document.getElementById('button-2').onclick = function(){
				let diametr = document.getElementById('inp1').value
				let color = document.getElementById('inp2').value
				document.getElementById('button-2').style = 'display:none;'
				document.getElementById('inp1').style = 'display:none;'
				document.getElementById('inp2').style = 'display:none;'
				
				let div = document.createElement('div')
				div.id = 'div-circle'
				document.body.appendChild(div)
				div.style.background = color;
				div.style.width = diametr + 'px'
				div.style.height = diametr + 'px'
				div.style.borderRadius = diametr/2 + 'px'
			}		
	}
}
circleCreate()